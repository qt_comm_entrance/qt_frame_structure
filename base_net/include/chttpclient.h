#ifndef HJ_FRAME_STRUCTURE_BASE_NET_INCLUDE_CHTTPCLIENT_H_
#define HJ_FRAME_STRUCTURE_BASE_NET_INCLUDE_CHTTPCLIENT_H_
#include "CBaseNetHeader.h"
#include "mongoose.h"
/**
 *
 * Copyright(c), SuperHouse Co.,Ltd.
 * All rights reserved.
 *
 * @brief   mqtt 客服端封装
 * @note    -
 * @author  lth
 * @date    2021-12-18
 *
 */

BASENET_BEGIN_NAMESPACE





/**
 * http请求方法
 *
 * @note
 * @return -
 */
typedef enum HTTP_Method {
	GET,
	POST,
	PUT,
	DEL,
}tsHttpMethod;

/**
 * http返回内容
 *
 * @note
 * @return -
 */
typedef struct ReqData
{
	// 返回码
	int code;
	// 返回内容
	string msg;
	// 请求状态
	bool status;
	// 返回头
	MAP_PARAMS parms;
	ReqData() {
		code = 0;
		msg="";
		status = false;

	}
}tsReqData;


class BASENET_API CHttpClient
{
public:

	/**
	 * 构造函数
	 *
	 * @note
	 * @return -
	 */
	CHttpClient();

	/**
	 * 析构函数
	 *
	 * @note
	 * @return -
	 */
	~CHttpClient();

private:
	/**
	 * 运行http请求
	 *
	 * @note
	 * @return -
	 */
	void requireRun();

	/**
	 * 发送请求数据
	 *
	 * @note
	 * @return -
	 */
	void sendReuireData(struct mg_connection *c, int ev, void *ev_data);


	/**
	 * 解析接受数据
	 *
	 * @note
	 * @return -
	 */
	void recvReuireData(struct mg_connection *c, int ev, void *ev_data);

	/**
	 * 块数据接受，大文件需要处理
	 *
	 * @note
	 * @return -
	 */
	void recvReuireChunkData(struct mg_connection *c, int ev, void *ev_data);

	/**
	 * 获取请求方法
	 *
	 * @note
	 * @return -
	 */
	const char *getHttpMethod();

public:

	/**
	 * http pool回调
	 *
	 * @note
	 * @return -
	 */
	void __httpClientCB(struct mg_connection *c, int ev, void *ev_data);

	/**
	 * 定时器回调
	 *
	 * @note
	 * @return -
	 */
	void __httpConTimeOutCB();
public:

	/**
	 * 清除http请求
	 *
	 * @note
	 * @return -
	 */
	void cancelReuire();

	/**
	 * 初始化连接参数
	 *
	 * @note
	 * @return -
	 */
	 void initContParm(tsHttpMethod method,const char *url,int conTimeout=3,int recvTimeout=5);

	/**
	 * 初始化数据参数
	 *
	 * @note
	 * @return -
	 */
	 void initDataParm(MAP_PARAMS *parm,MAP_PARAMS *extra_headers=NULL,string data="",string fileName="",string filePath="");

	/**
	 * 发起请求
	 *
	 * @note
	 * @return -
	 */
	 void initTls(struct mg_tls_opts *sTlsOpts);

	/**
	 * 发起请求
	 *
	 * @note
	 * @return -
	 */
	 tsReqData startReuire();

private:
	// 连接超时
	int m_iConTime;
	// 接收超时
	int m_iRecvTime;
	// 请求头
	MAP_PARAMS *m_pHeadParams;
	// 请求参数
	MAP_PARAMS *m_pParms;
	// 请求内容(字符串)
	string m_tBody;
	// 请求内容（文件名）
	string m_sFileName;
	// 请求内容（文件路径）
	string m_sFilePath;
	// 请求地址
	string m_sUrl;
	// 请求方法
	tsHttpMethod m_sMethod;
	// 运行状态
	bool m_bRunStatus;
	// http返回内容
	tsReqData m_sReqData;
	// 计算时间
	unsigned long m_lPrev_second;

private:
	// 连接超时计数
	int m_iConTimeTmp;
	// 连接文件
	struct mg_connection *m_sCon;
	// 连接管理
	struct mg_mgr m_sMgr;
	// ssl配置
	struct mg_tls_opts m_sTlsOpts;

public:
	// 下载文件
	static tsReqData downFile(const char *url,const char *savePath,int conTimeout=3,int recvTimeout=10);
	// 上传文件
	static tsReqData pullFile(tsHttpMethod method,const char *url,MAP_PARAMS *parms,MAP_PARAMS *headParams,string fileName,string filePath,int conTimeout=3,int recvTimeout=60);
	// http请求
	static tsReqData httpReq(tsHttpMethod method,const char *url,MAP_PARAMS *parms,MAP_PARAMS *headParams=NULL,string data="",int conTimeout=3,int recvTimeout=10);

	/**
	*  @brief http连接请求 (只测试服务器能否连接)
	*  @param[IN] strUrl url地址
	*  @param[IN] connectTimeout 连接超时
	*  return 0 成功
	*/
	static bool getServerIsSuccess(const std::string &strUrl, int connectTimeout=3000);
};


BASENET_END_NAMESPACE





#endif // HJ_FRAME_STRUCTURE_BASE_NET_INCLUDE_CHTTPCLIENT_H_
