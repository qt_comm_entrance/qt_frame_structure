#ifndef HJ_FRAME_STRUCTURE_BASE_NET_INCLUDE_CBASENETHEADER_H_
#define HJ_FRAME_STRUCTURE_BASE_NET_INCLUDE_CBASENETHEADER_H_

#include <QtCore/qglobal.h>
#include <string.h>
#include <base_lib/CBaseLibSdk.h>
#include <QThread>

BASELIB_USE_NAMESPACE

using namespace std::placeholders;
using namespace std;


#if 1
#define MAP map
#else
#define MAP unordered_map
#endif
#define MAP_PARAMS MAP<string, string>

#ifdef BASE_NET_LIBRARY
#		define BASENET_API Q_DECL_EXPORT
#		define BASE_NET_EXPORT Q_DECL_EXPORT
#else
#		define BASENET_API Q_DECL_IMPORT
#		define BASE_NET_EXPORT Q_DECL_IMPORT
#endif


//命名空间宏定义
#define BASENET_NAMESPACE _base_net
#define BASENET_USE_NAMESPACE using namespace ::BASENET_NAMESPACE;
#define BASENET_BEGIN_NAMESPACE namespace BASENET_NAMESPACE {
#define BASENET_END_NAMESPACE }

#endif // HJ_FRAME_STRUCTURE_BASE_NET_INCLUDE_CBASENETHEADER_H_
