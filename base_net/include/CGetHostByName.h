/*
 * CGetHostByName.h
 *
 *  Created on: 2023年3月18日
 *      Author: jxs
 *      Text: 域名解析
 */

#ifndef HJ_FRAME_WORK_BASE_NET_CGETHOSTBYNAME_H_
#define HJ_FRAME_WORK_BASE_NET_CGETHOSTBYNAME_H_

#include "CBaseNetHeader.h"
#include "mongoose.h"
#include <QMutex>


BASENET_BEGIN_NAMESPACE

class BASENET_API CGetHostByName {
public:

    CGetHostByName();

    virtual ~CGetHostByName();

    // 获取主机地址
    string getHostName(const string &url, int timeOutMs = 1000);

    // 获取主机地址
    static string getHostByName(const string &url, int timeOutMs = 1000);

    // 线程获取IP
    static void* __threadGetHostName(void*);

    // 初始化mongoose库中的通用回调
    static void initMongoose();

private:

    // 域名对应IP
    static MAP<std::string, std::string> m_hostAndNameMap;
    // 当前已开线程计数
    static int m_currentThreadCount;
    // 互斥锁
    static QMutex m_lock;

};

BASENET_END_NAMESPACE


#endif /* HJ_FRAME_WORK_BASE_NET_CGETHOSTBYNAME_H_ */
