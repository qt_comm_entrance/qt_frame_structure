include(../qt_frame_structure.pri)

BASE_NET_BASE = ..
TEMPLATE = lib
DEFINES += BASE_NET_LIBRARY

TARGET = base_net

!win32:VERSION = 0.1.0

QT += network
QT += core
QT -= gui

CONFIG += dll debug_and_release lib



CONFIG(debug, debug|release) {
    win32: TARGET = $$join(TARGET,,,d)
}


OBJECTS_DIR = $$BASE_NET_BASE/build
MOC_DIR = $$BASE_NET_BASE/build

INCLUDEPATH += \
    include

HEADERS =  \
    include/CBaseNetHeader.h \
    include/CBaseNetSdk.h \
    include/CGetHostByName.h \
    include/CHttpFile.h \
    include/CHttpService.h \
    include/CMqttClient.h \
    include/CHttpClient.h \
    include/mongoose.h

SOURCES = *.cpp \
    mongoose.c


LIBS += -lmbedcrypto -lmbedtls -lmbedx509 -lbase_lib


target.path = $$DLLDESTDIR
INSTALLS += target



#拷贝公共头文件到发布目录
win32 {
    COPY_SRC = $$replace(PWD, /, \\)
    COPY_DEST = $$replace(PUBLIC_LIB_DIR, /, \\)
    system("mkdir $$COPY_DEST\\include\\base_net\\")
    system("copy /y $$COPY_SRC\\include\\*.h $$COPY_DEST\\include\\base_net\\")
}
else {
    system("mkdir $$PUBLIC_LIB_DIR/include/base_net/")
    system("cp $$PWD/*.h $$PUBLIC_LIB_DIR/include/base_net/")
}
