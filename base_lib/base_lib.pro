include(../qt_frame_structure.pri)

BASE_LIB_BASE = ..
TEMPLATE = lib
DEFINES += BASE_LIB_LIBRARY

TARGET = base_lib

!win32:VERSION = 0.1.0

QT += network
QT += core
QT -= gui

CONFIG += dll debug_and_release lib



CONFIG(debug, debug|release) {
    win32: TARGET = $$join(TARGET,,,d)
}

INCLUDEDIR += ./include

OBJECTS_DIR = $$BASE_LIB_BASE/build
MOC_DIR = $$BASE_LIB_BASE/build

HEADERS = ./include \
    include/CBaseHeader.h \
    include/CBaseLibSdk.h \
    include/CMultipartParser.h \
    include/CUtils.h

SOURCES = *.cpp \




target.path = $$DLLDESTDIR
INSTALLS += target



#拷贝公共头文件到发布目录
win32 {
    COPY_SRC = $$replace(PWD, /, \\)
    COPY_DEST = $$replace(PUBLIC_LIB_DIR, /, \\)
    system("mkdir $$COPY_DEST\\include\\base_lib\\")
    system("copy /y $$COPY_SRC\\include\\*.h $$COPY_DEST\\include\\base_lib\\")
}
else {
    system("mkdir $$PUBLIC_LIB_DIR/include/base_lib/")
    system("cp $$PWD/include/*.h $$PUBLIC_LIB_DIR/include/base_lib/")
}
