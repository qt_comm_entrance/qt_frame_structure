/**************************************************************************
Date: 2020-08-11
Description: 公共头文件
**************************************************************************/
#ifndef HJ_FRAME_STRUCTURE_BASE_LIB_INCLUDE_CBASEHEADER_H_
#define HJ_FRAME_STRUCTURE_BASE_LIB_INCLUDE_CBASEHEADER_H_

#include <QtCore/QtGlobal>

#ifdef BASE_LIB_LIBRARY
#		define BASELIB_API Q_DECL_EXPORT
#		define BASE_LIB_EXPORT Q_DECL_EXPORT
#else
#		define BASELIB_API Q_DECL_IMPORT
#		define BASE_LIB_EXPORT Q_DECL_IMPORT
#endif



//命名空间宏定义
#define BASELIB_NAMESPACE _base_lib
#define BASELIB_USE_NAMESPACE using namespace ::BASELIB_NAMESPACE;
#define BASELIB_BEGIN_NAMESPACE namespace BASELIB_NAMESPACE {
#define BASELIB_END_NAMESPACE }



#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <map>
#include <unordered_map>
#include <vector>
#include <queue>
#include <list>
#include <memory>
#include <string>
#include <algorithm>
#include <functional>
#include <cctype>
#include <time.h>
#include <stdarg.h>
#include <assert.h>
#include <string.h>
#include <atomic>
#include <future>
#include <stdexcept>

using namespace std;


BASELIB_BEGIN_NAMESPACE

#define CHECK_AND_DELETE(pointer) if(pointer) delete pointer; pointer = NULL;
#define CHECK_AND_FREE(pointer)   if(pointer) free pointer; pointer = NULL;

#ifdef _WINDOWS
#define ERASE(obj, it) it = obj.erase(it)
#else
#define ERASE(obj, it) obj.erase(it++)
#endif

// 单例宏定义 eg. 头文件示例： static class_name &Instance();   源文件示例：INSTANCE_IMP(class_name);
#define INSTANCE_IMP(class_name, ...) \
class_name &class_name::Instance() { \
    static std::shared_ptr<class_name> s_instance(new class_name(__VA_ARGS__)); \
    static class_name &s_insteanc_ref = *s_instance; \
    return s_insteanc_ref; \
}


BASELIB_END_NAMESPACE



#endif // HJ_FRAME_STRUCTURE_BASE_LIB_INCLUDE_CBASEHEADER_H_
