﻿/**************************************************************************
Date: 2020-08-11
Description: 系统工具类
**************************************************************************/
#ifndef HJ_FRAME_STRUCTURE_BASE_LIB_INCLUDE_CUTILS_H_
#define HJ_FRAME_STRUCTURE_BASE_LIB_INCLUDE_CUTILS_H_

#include "CBaseHeader.h"
BASELIB_BEGIN_NAMESPACE


#include <iostream>

typedef unsigned short WORD;
typedef unsigned char BYTE;
#define OUTLEN 255

#pragma pack(2)   			/* 取消字节对齐 */


#pragma pack() 	/* 恢复字节对齐 */


typedef struct PACKED         //定义一个cpu occupy的结构体
{
char name[20];      //定义一个char类型的数组名name有20个元素
unsigned int user; //定义一个无符号的int类型的user
unsigned int nice; //定义一个无符号的int类型的nice
unsigned int system;//定义一个无符号的int类型的system
unsigned int idle; //定义一个无符号的int类型的idle
}CPU_OCCUPY;

class BASELIB_API CUtils {
public:
	/**
	* 私有化构造函数
	*
	*/
	CUtils();

	/**
	* 析构函数
	*
	* @param[in] -
	* @note -
	* @return -
	*/
	~CUtils();

public:
	/**
	* 获取cpu串号
	*
	* @param[out] strCpuId
	* @note -
	* @return bool
	*/
    static bool getCpuId(string& strCpuId);
	/**
	* 判断IP地址的合法性
	*
	* @param[in] pcIp IP地址
	* @note -
	* @return 合法发挥true
	*/
    static bool isValidIP(string pcIp);


	/* 获取网络配置模式
	* @param[out] isDhcp  DHCP状态
	* @note -
	* @return
	*/
	static bool isDhcp(bool *isDhcp);

	/* MAC地址是否合法
	* @param[out] mac  mac
	* @note -
	* @return
	*/
	static bool checkMacAddr(const char *mac);


	/*
	* 创建多级目录
	* @param[in] sPathName  目录路径
	* @note -
	* @return 0 成功； 非0 失败
	*/
	static int createMultiLevelDir(char* sPathName);
	/*
	* 16进制转字符串
	* @param[in] pucBuf 16进制buffer
	* @param[in] usLen buffer长度
	* @note -
	* @return 16进制字符串
	*/
	static string hexToString(uint8_t *pucBuf, uint16_t usLen, bool isCapitalize = true);
	/*
	* 字符串转16进制
	* @param[in] str 16进制字符串
	* @param[out] pucBuf 输出16进制buffer
	* @param[in] usLen buffer长度
	* @note -
	* @return 
	*/
	static uint16_t stringToHex(string *str, uint8_t *pucBuf, uint16_t usLen);
	/*
	* 字符串16进制转int
	* @param[in] s 16进制字符串
	* @note -
	* @return int
	*/
	static int htoi(unsigned char *s);
	/**
	* rec_ucCheckXor 异或校验
	* 参数：*pBuf——当前数据缓冲区指针
	*       len——数据长度
	* 返回：计算结果
	**/
	static uint8_t ucBufCheckXor(uint8_t *pucBuf, uint16_t usLen);
	/**
	* rec_check sum 求和
	* 参数：*pBuf——当前数据缓冲区指针
	*       len——数据长度
	* 返回：计算结果
	**/
	static uint8_t ucBufCheckSum(uint8_t *pucBuf, uint16_t usLen);
	/**
	* rec_check sum 求和
	* 参数：*pBuf——当前数据缓冲区指针
	*       len——数据长度
	* 返回：计算结果
	**/
	static uint16_t ucBufCheckSum16(uint8_t *pucBuf, uint16_t usLen);
	/**
	* rec_check sum 求和
	* 参数:*pBuf当前数据缓冲区指针
	*       len数据长度
	* 返回:计算结果
	**/
	static uint32_t uiBufCheckSum(uint8_t *pucBuf, uint16_t usLen);

	/*
	* 字符串替换
	* @param[in] resources 需要替换的字符串
	* @param[in] key	       需要替换的内容
	* @param[in] ReplaceKey 替换的内容
	* @note -
	* @return string
	*/
	static string strReplaceAllword(const string& resources,const string &key, const string& ReplaceKey);

	/*
	* 去掉转义符
	* @param[in] resources 需要替换的字符串
	* @note -
	* @return
	*/
	static void unescapeC(string &resources);

	//字符串分割函数
	static std::vector<std::string> split(std::string str, std::string pattern);

	// list转字符串
	static string listToStdString(const list<string> & listAddr, const string &pattern = ",");

	// 字符串转list
	static list<string> stdStringToList(string listAddr, const string &pattern = ",");

	/*
	* 数字转string
	* @param[in] a_value 需要转换的内容
	* @param[in] n	      保留小数点后几位
	* @note -
	* @return string
	*/
	static string toString(const float a_value, const int n);
	static string toString(const double a_value, const int n);

	// 获取唯一ID
	static string getOnlyId();

	// 获取不固定长度的唯一ID
	static string getLongOnlyId();

	/*
	* 字符串截取
	* @param[in] src 需要截取的字符串
	* @param[in] findValue 起始字符串
	* @param[in] stopName 截止字符串
	* @note - 遇到findValue后，在findValue之后开始截取，直到遇到stopName
	* @note - 如果stopName为空就说明是截取后面全部
	* @note - 如果findValue为空就从头开始截取
	* @note - 如果两个都是空的就返回空
	* @note - 如果findValue不为空且没找到就返回空
	* @note - 如果stopName不为空且没找到就返回空
	* @return
	*/
	static string SubString(string src, string findValue, string stopName);

	// 字符串替换
	static void replace_all_distinct(string& str, string old_value, string new_value);

	// 字符删除
	static void removeChar(string &str, char removeStr);

	// 字符串删除
	static void removeString(string &str, string removeStr);

	// 统计字符串出现次数
	static unsigned long StatisticsStrCount(string src, string statisticsStr);

	// 时间字符串转时间戳
	static long timerStrToTimeStamp(string timerStr);

	/*
	* 字符串截取
	* @param[in] src 输入字符串
	* @param[in] deleText 需要从头开始删除的字符串，支持带*通配符
	* @return	返回去掉deleText之后的字符串
	*/
	static void deleteBase64Text(string& src, const string &deleText);

	// 编码为base64
	static void encodeBase64(const char *src, string &dst);

	// 编码为base64
	static void encodeBase64(const char *src, int srcLen, string &dst);

	// 文件编码为base64
	static void encodeFileToBase64(string filaPath, string &dst);

	// 解码base64
	static void decodeBase64(const char *src, string &dst);

	// 解码base64到文件
	static bool decodeBase64ToFile(const char *src, string filePath);

	// 删除文件或文件夹
	static void deletePath(const char* path);

	// 路径检查，不存在就创建
	static void pathInspect(const string &path);

	// 对比两次cpu信息得到cpu使用情况
	static int calCpuoccupy (CPU_OCCUPY *o, CPU_OCCUPY *n);

	// 获取cpu信息
    static bool getCpuoccupy (CPU_OCCUPY *cpust);

	// 获取文件大小
	static long long get_file_size(const std::string filename);

    // 生成随机字符串
    static string randomString(int length);
};


BASELIB_END_NAMESPACE

#endif // HJ_FRAME_STRUCTURE_BASE_LIB_INCLUDE_CUTILS_H_
