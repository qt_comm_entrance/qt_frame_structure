#ifndef HJ_FRAME_STRUCTURE_BASE_LIB_INCLUDE_CMULTIPARTPARSER_H_
#define HJ_FRAME_STRUCTURE_BASE_LIB_INCLUDE_CMULTIPARTPARSER_H_

#include "CBaseHeader.h"


BASELIB_BEGIN_NAMESPACE


class BASELIB_API CMultipartParser
{
public:
	CMultipartParser();
	~CMultipartParser();

	inline const std::string &body_content()
	{
		return body_content_;
	}
	inline const std::string &boundary()
	{
		return boundary_;
	}
	inline void addParameter(const std::string &name, const std::string &value)
	{
		params_.push_back(std::move(std::pair<std::string, std::string>(name, value)));
	}
	inline void addFile(const std::string &name, const std::string &value)
	{
		files_.push_back(std::move(std::pair<std::string, std::string>(name, value)));
	}
	const std::string &genBodyContent();

private:
	void _get_file_name_type(const std::string &file_path, std::string *filenae, std::string *content_type);
private:
    static const std::string boundary_prefix_;
    static const std::string rand_chars_;
	std::string boundary_;
	std::string body_content_;
	std::vector<std::pair<std::string, std::string> > params_;
	std::vector<std::pair<std::string, std::string> > files_;
};


/*
CMultipartParser parser;
parser.addParameter("file_name", "1.jpg");
parser.addFile("file", "1.jpg");
std::string boundary = parser.boundary();
std::string body = parser.genBodyContent();
*/


BASELIB_END_NAMESPACE

#endif // HJ_FRAME_STRUCTURE_BASE_LIB_INCLUDE_CMULTIPARTPARSER_H_
